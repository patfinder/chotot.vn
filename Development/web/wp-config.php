<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'chotot');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'Abcd123$');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '9JZ{R6,^ZG-^yXe:E?q>cd:2,f:Xt[8%I9Jj_^+RIl1Eq~~43|,CnL1Cai%izOfl');
define('SECURE_AUTH_KEY',  'w)G<!$;*zb|DtPS-MEccZra15iy3<%[i{koR8R9]Ig.3),6VuR})Fy mqw0,J[xX');
define('LOGGED_IN_KEY',    ':%_F-)qGX$.f`?v*kl2<I/m5cxW[l,rdPUr9avbygxmy4Yj7A|S+y@+;q4lj9C|,');
define('NONCE_KEY',        'd?Y{KqJ%V!DC[^<FJEnVXr}z_t3`$f8>dfci|_`*@+Cfy]w9>t{&?|dW5<2ME43N');
define('AUTH_SALT',        'xPuny.K+}VVtqtm9)R?abWOWoe;aWq*!+|eU^T*uMC+t+fi31!-a76pr|4|*0DY{');
define('SECURE_AUTH_SALT', 'GvrZw0i`sG!kMOzVnE:y-_KI->E.-4=&0dhBi2/mT(Wfl.{.m%2ZwK-|8l#}9uPh');
define('LOGGED_IN_SALT',   'b93|X;Dj[B59`<|X,B+M00/+1_4184Cp?uNEg,t|#5c?MRx|()-LM,]<ZGXC<7th');
define('NONCE_SALT',       '|q!Vu2cJ`[+Q6]]*o%hdgO2;?#^jar@-W7+@QNrzi`rS`_o[:i+URIYL55%I[`_s');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
