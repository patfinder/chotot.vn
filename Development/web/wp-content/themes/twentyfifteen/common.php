<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 8/8/2015
 * Time: 5:20 PM
 */

function chotot_menu() {
    wp_nav_menu( array(
        'menu' => 'Main Menu',
        'container' => 'nav',
        'container_class' => 'col-md-9 navbar navbar-default',
        'menu_class' => 'nav navbar-nav',
        // do not fall back to first non-empty menu
        'theme_location' => '__no_such_location',
        // do not fall back to wp_page_menu()
        'fallback_cb' => false
    ) );
}

function get_logo() {
    $logo = get_page_by_title(get_option('site_logo'), OBJECT, 'attachment');
    return $logo ? $logo->guid : get_template_directory_uri() . "/img/logo.png";
}

function get_media_by_title($title) {
    return get_page_by_title($title, OBJECT, 'attachment')->guid;
}