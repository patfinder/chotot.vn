<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

//include "test-mail.php";
//die();

require_once("common.php");

$refer_url = wp_get_referer() ? wp_get_referer() : "/career";

// --------------------- Get job Info ---------------------

// Get post info
$job_id = get_query_var("job-id");

if($job_id > 0) {
	$post = get_post($job_id);
	if (!$post) {
		wp_redirect(get_home_url(false, "not-found"));
	}

	$job_title = $post->post_title;

	// Get job type, expired time
	$custom_fields = get_post_custom($job_id);
	$job_type = $custom_fields['Job Type'][0];
	$expired_time = $custom_fields['Expired Time'][0];
} else {
	$job_title = "Apply for Possible Jobs";
}

// --------------------- Process Form Submission ---------------------

//If the form is submitted
if( isset( $_POST['submitted'] ) ) {

	$submitted = true;

	//Check to make sure that the name field is not empty
	if( trim( $_POST['candidate-name'] ) === '' ) {
		$candidateNameError =  __( 'You forgot to enter your name.', 'woothemes' );
	} else {
		$candidateName = trim( $_POST['candidate-name'] );
	}

	//Check to make sure sure that a valid email address is submitted
	if( trim( $_POST['email'] ) === '' )  {
		$emailError = __( 'You forgot to enter your email address.', 'woothemes' );
	} else if ( ! eregi( "^[A-Z0-9._%-]+@[A-Z0-9._%-]+\.[A-Z]{2,4}$", trim($_POST['email'] ) ) ) {
		$emailError = __( 'You entered an invalid email address.', 'woothemes' );
	} else {
		$email = trim( $_POST['email'] );
	}

	//Check if phone number is valid
	if ( $_POST['phone-number'] && ! eregi( "^[(]?[0-9]{3}[)]?[- .]?[0-9]{3}[- .]?[0-9]{4}$", trim($_POST['phone-number'] ) ) ) {
		$phoneNumberError = __( 'You entered an invalid phone number.', 'woothemes' );
	} else {
		$phoneNumber = trim( $_POST['phone-number'] );
	}

	$resume = $_FILES['resume'] && $_FILES['resume']['tmp_name'] ? array($_FILES['resume']['tmp_name']) : array();
	$letter = $_FILES['letter'] && $_FILES['letter']['tmp_name'] ? array($_FILES['letter']['tmp_name']) : array();
	$files = array_merge($resume, $letter);

	if( empty($resume) )  {
		$resumeError = __( 'You forgot to provide resume.', 'woothemes' );
	}

	$errors = array_merge(
		$resumeError ? array($resumeError) : array(),
		$candidateNameError ? array($candidateNameError) : array(),
		$phoneNumberError ? array($phoneNumberError) : array(),
		$emailError ? array($emailError) : array());

	//If there is no error, send the email
	if( empty($errors) ) {

		$appliedJob = "\n\nApplied Job: " . $job_title;

		$emailTo = get_option( 'admin_email' );
		$subject = __( 'Candidate Apply', 'woothemes' ) . " - $candidateName - $job_title";
		$body = "Name: $candidateName \n\nEmail: $email \n\nPhone No: $phoneNumber " . $appliedJob;
		$headers = __( 'From: ', 'woothemes') . "$candidateName <$email>" . "\r\n" . __( 'Reply-To: ', 'woothemes' ) . $email;

		$mailSent = wp_mail( $emailTo, $subject, $body, $headers, $files);
	}
}

// --------------------- Start Page Content ---------------------

get_header();

?>


	<!--[if lt IE 8]>
	<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->

	<!-- Add your site or application content here -->
	<header class="container-fluid header">
		<div class="row content-center">
			<div class="col-md-3">
				<a href="/" class="logo" style="background:url('<?php echo get_logo(); ?>')">ChoTot</a>
			</div>
			<?php chotot_menu(); ?>
		</div>

	</header>
	<main class="container-fluid sub-page">
		<div class="row content-center">
			<section class="col-md-8 form-apply main-content">
				<div class="content">
					<div class="header-pager">
						<a href="<?php echo $refer_url; ?>" class="link-black"><span class="icon-back"></span>Back to job</a>
						<h1><?php echo $job_title ?></h1>
						<?php if($job_id > 0)  { ?>
						<p class="position"><?php echo($job_type); ?>. Expired: <?php echo($expired_time); ?></p>
						<?php } else { ?>We will get back to you soon.<?php } ?>
					</div>

					<?php if($submitted && empty($errors)) {
						// Email processed
						if($mailSent) { ?><div style="color: #FF6666;"><strong>Your Job Application has been sent. We will contact you soon.</strong></div>
						<?php } else { ?>
							<div style="color: #FF6666;"><strong>Failed to send Job Application email. Please try again later.</strong></div>
						<?php }
					} else { ?>

					<form action="<?php echo(home_url( add_query_arg( NULL, NULL ) ) ); ?>"
						  id="job-form" method="POST" enctype="multipart/form-data">
						<?php if(!empty($errors)) { ?>
						<div class="form-group" style="color: #FF6666;">
							<span>Errors:</span>
							<ul>
							<?php foreach($errors as $error) { ?>
								<li class="error"><?php echo $error; ?></li>
							<?php } ?>
							</ul>
						</div>
						<?php } ?>
						<div class="form-group">
							<label for="cv">ATTACH YOU CV/RESUME</label>
							<input type="file" id="resume" name="resume">
							<!--<input type="text" class="form-control" id="resume" name="resume" placeholder="">-->
						</div>
						<div class="form-group">
							<label for="name">NAME</label>
							<input type="text" class="form-control" id="candidate-name" name="candidate-name" value="<?php echo $candidateName; ?>" placeholder="">
						</div>
						<div class="column-2 clearfix">
							<div class=" col-md-6 form-group">
								<label for="phone-number">PHONE NUMBER</label>
								<input type="text" class="form-control" id="phone-number" name="phone-number" value="<?php echo $phoneNumber; ?>" placeholder="">
							</div>
							<div class=" col-md-6 form-group">
								<label for="email">EMAIL ADDRESS</label>
								<input type="text" class="form-control" id="email" name="email" value="<?php echo $email; ?>" placeholder="">
							</div>
						</div>

						<div class="form-group">
							<label for="letter">COVER LETTER/INTRODUCTION</label>
							<input type="file" id="letter" name="letter">
							<input type="hidden" name="letter_file_name" id="letter_file_name" />
							<?php //echo do_shortcode( '[wordpress_file_upload uploadpath="intro-letters" uploadtitle=""]' ); ?>
						</div>

						<input type="hidden" name="submitted" id="submitted" value="true" />
						<button type="submit" class="btn btn-default btn-apply">APPLY NOW</button>
					</form>
					<?php } ?>

				</div>
			</section>

		</div>
	</main>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script type="text/javascript" src="http://cdn.jsdelivr.net/jquery.slick/1.5.0/slick.min.js"></script>

	<!--<script type="text/javascript">
		function afterUpload(success) {
			if(!success) {
				return;
			}
			// success
			var file_name = $("#wordpress_file_upload_block_1 #fileName_1")[0].value
			$("#letter_file_name").val(file_name);
		}
	</script>-->

	<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
	<script>
		(function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
			function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
			e=o.createElement(i);r=o.getElementsByTagName(i)[0];
			e.src='https://www.google-analytics.com/analytics.js';
			r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
		ga('create','UA-XXXXX-X','auto');ga('send','pageview');
	</script>

<?php get_footer(); ?>