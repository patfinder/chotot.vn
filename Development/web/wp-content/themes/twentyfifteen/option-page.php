<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 8/7/2015
 * Time: 8:58 PM
 */
?>

<script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.js"></script>
<script type="text/javascript" src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<div class="wrap">
    <h2>Chotot Options Page</h2>
    <hr style="width: 85%" />

    <br /><div style="clear: both;"><strong>Sections Order (move to rearrange)</strong></div><br />

    <ul id="sortable">
    <?php

        $sectionNames = array(
            0 => "",
            1 => "Life at Chotot",
            2 => "Reasons Why ChoTot Is For You",
            3 => "Testimony",
            4 => "Join Our Family"
        );

        $sections = get_option('sections_order');
        if(empty($sections)) $sections = "1,2,3,4";
        $sections = explode(",", $sections);

        foreach($sections as $s) {
    ?>
        <li class="ui-state-default chotot-section" id="section-<?php echo $s ?>"><span class="ui-icon ui-icon-arrowthick-2-n-s"></span><?php echo $sectionNames[$s] ?> (<?php echo $s ?>)</li>
    <?php } ?>
    </ul>

    <form method="post" action="options.php">
        <?php settings_fields( 'chotot-plugin-settings-group' ); ?>
        <?php do_settings_sections( 'chotot-plugin-settings-group' ); ?>
        <table class="form-table">
            <tr valign="top">
                <th scope="row">Site Logo Name (Media Name)</th>
                <td><input type="text"  style="width: 600px;" name="site_logo" value="<?php echo esc_attr( get_option('site_logo') ); ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row">Banner Message</th>
                <td><input type="text" style="width: 600px;"  name="banner_message" value="<?php echo esc_attr( get_option('banner_message') ); ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row">Life At Chotot Title</th>
                <td><input type="text" style="width: 600px;"  name="life_at_chotot_title" value="<?php echo esc_attr( get_option('life_at_chotot_title') ); ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row">Life At Chotot Sub Title</th>
                <td><input type="text" style="width: 600px;"  name="life_at_chotot_sub_title" value="<?php echo esc_attr( get_option('life_at_chotot_sub_title') ); ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row">Benefits of Chotot Title</th>
                <td><input type="text" style="width: 600px;"  name="benefit_of_chotot_title" value="<?php echo esc_attr( get_option('benefit_of_chotot_title') ); ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row">Testimonial Background (Media Name)</th>
                <td><input type="text" style="width: 600px;"  name="testimonial_background" value="<?php echo esc_attr( get_option('testimonial_background') ); ?>" /></td>
            </tr>

            <input type="hidden" id="sections_order" name="sections_order" value="<?php echo esc_attr( get_option('sections_order') ); ?>" />

            <!--<tr valign="top">
                <th scope="row">Benefits of Chotot Sub Title</th>
                <td><input type="text" style="width: 600px;"  name="benefit_of_chotot_sub_title" value="<?php /*echo esc_attr( get_option('benefit_of_chotot_sub_title') ); */?>" /></td>
            </tr>-->

        </table>

        <?php submit_button(); ?>

    </form>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $("#sortable").sortable();
        $("#sortable").disableSelection();

        // Save item section order
        var sections = $(".chotot-section");
        sections.on("mouseup", function(){

            // Map section, order
            var sectionOrders = {};

            setTimeout(doSave, 0);
        });

        function doSave() {

            sectionOrders = {};

            sections.each(function(){
                $this = $(this);
                console.log("item: " + $this.attr("id").substr(8) + " - " + $this.index());
                // section-n / order = index
                sectionOrders[$this.attr("id").substr(8)] = $this.index();
            });

            var tuples = [];
            for (var key in sectionOrders) tuples.push([key, sectionOrders[key]]);

            tuples.sort(function(a, b) {
                a = a[1];
                b = b[1];

                return a < b ? -1 : (a > b ? 1 : 0);
            });

            list1 = [];
            for (var i = 0; i < tuples.length; i++) {
                var key = tuples[i][0];
                var value = tuples[i][1];

                // do something with key and value
                list1.push(key);
            }

            list = list1.join();
            $("#sections_order").val(list);
            // console.log("Items Order: " + list);
        }

        $("<link/>", {
            rel: "stylesheet",
            type: "text/css",
            href: "<?php echo get_template_directory_uri() ?>/css/admin-style.css"
        }).appendTo("head");

        $("<link/>", {
            rel: "stylesheet",
            type: "text/css",
            href: "<?php echo get_template_directory_uri() ?>/css/jquery-ui.css"
        }).appendTo("head");
    });
</script>
