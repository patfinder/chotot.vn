<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

require_once("common.php");

get_header();


// $args = array(
// 	'type' => 'post',
// 	'category_name' => 'Testimony',
// 	'orderby' => 'RAND()'
// );
// $testimony = array_values(get_posts($args));
// if(count($testimony) > 0)
// 	$testimony = $testimony[0];
//
// echo "<pre>";
// print_r($testimony);
// echo "</pre>";
// die();

// --------------------------- Jobs List ---------------------------

// job-type is Job category Name
$job_type = get_query_var("job-type");

// Default: all jobs
if(!$job_type)
	$job_type = 'Job Posting';

$args = array(
	'type' => 'post',
	'category_name' => $job_type
);
$job_list = array_values(get_posts($args));

// --------------------------- Job Category Images ---------------------------

// cat-id, icon-id mapping
$cat_icon = array(
	3 => 54, // product
	6 => 53, // hr
	7 => 52, // others
	4 => 51, // marketing
	5 => 50, // content
	2 => 49 // job-posting, default
);

$args = array(
	'post_type' => 'attachment',
	'post__in' => array_values($cat_icon)
);
$icon_list = get_posts($args);

// --------------------------- Job Category Info ---------------------------

$args = array(
	'type'                     => 'post',
	// 'child_of'                 => 0,
	'parent'                   => 2,
	'orderby'                  => 'ID',
	'order'                    => 'ASC',
	'hide_empty'               => 1,
	'hierarchical'             => 0,
	'exclude'                  => '',
	'include'                  => '',
	'number'                   => '',
	'taxonomy'                 => 'category',
	'pad_counts'               => false
);

$jobCategories = get_categories( $args );

// --------------------------- Page Images ---------------------------

// get_stylesheet_directory_uri() . "/";
$theme_base = get_template_directory_uri() . "/";

$attachedImages = get_attached_media('image', get_the_ID());

// Get life images and reason images
$lifeImages = array();
$reasonImages = array();

foreach($attachedImages as $img) {
	if(strpos($img->post_title, "life-") !== false)
		array_push($lifeImages, $img);
	else if(strpos($img->post_title, "reason-") !== false)
		array_push($reasonImages, $img);
}

// Life images
usort(
	$lifeImages,
	function($a, $b) {
		return strcmp($a->post_title, $b->post_title);
	});

// Reason images
usort(
	$reasonImages,
	function($a, $b) {
		return strcmp($a->post_title, $b->post_title);
	});

// --------------------------- Testimony ---------------------------

$args = array(
	'type' => 'post',
	'category_name' => 'Testimony',
	'orderby' => 'rand'
);
$testimony = array_values(get_posts($args));
if(count($testimony) > 0)
	$testimony = $testimony[0];

// --------------------------- Start of HTML ---------------------------

?>
	<!--[if lt IE 8]>
	<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->

	<!-- Add your site or application content here -->
	<header class="parallax-header container-fluid" data-parallax="scroll" data-image-src="<?php echo $theme_base; ?>img/banner-header.jpg">
		<div class="row content-center">
			<div class="col-md-3">
				<a href="/" class="logo" style="background:url('<?php echo get_logo(); ?>')">ChoTot</a>
				<span class="career">Career</span>
			</div>
			<?php chotot_menu(); ?>
			<div class="col-md-12">
				<h1> “<?php echo get_option("banner_message") ?>”
				</h1>
				<a href="#JoinFamily" class="btn">View open positions</a>
			</div>
		</div>

	</header>
	<main>

		<?php

		$sections = get_option('sections_order');
		if(empty($sections)) $sections = "1,2,3,4";
		$sections = explode(",", $sections);

		$count = count($sections);
		for($isec = 0; $isec < $count; $isec++) {

			$sec = $sections[$isec];

			if($sec == "1") {
		?>
		<!--------------------------------- Life Section --------------------------------->
		<section class="container-fluid life-section">
			<div class="row content-center-large">
				<div class="col-md-12">
					<h2><?php echo get_option('life_at_chotot_title') ?></h2>
					<p><?php echo get_option('life_at_chotot_sub_title') ?></p>
				</div>
				<div class="slide-life clearfix">
					<?php

					$count = count($lifeImages);
					for($i = 0; $i < $count ; $i += 2) {
						$path1 = $lifeImages[$i]->guid;
						// post_title / post_excerpt
						$title1 = $lifeImages[$i]->post_excerpt;
						if($i + 1 < $count) {
							$path2 =  $lifeImages[$i + 1]->guid;
							$title2 = $lifeImages[$i + 1]->post_excerpt;
						} else {
							$path2 = $title2 = "";
						}
					?>
					<div class="items clearfix">
						<div class="col-md-12">
							<figure>
								<img src="<?php echo $path1; ?>" alt="#">
								<figcaption class="content-hover">
									<a href=""><?php echo $title1; ?></a>
								</figcaption>
							</figure>
							<figure>
								<img src="<?php echo $path2; ?>" alt="#">
								<figcaption class="content-hover">
									<a href=""><?php echo $title2; ?></a>
								</figcaption>
							</figure>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</section>

		<?php
		} else if($sec == "2") {
			?>

		<!--------------------------------- Benefits Section --------------------------------->
		<section class="container-fluid reasons-section">
			<div class="row content-center">
				<h2><?php echo get_option('benefit_of_chotot_title') ?></h2>
				<?php
					$count = count($reasonImages);
					for($i = 0; $i < $count ; $i += 3) {
				?>
				<?php
					for($j = 0; $j < 3 && $i + $j < $count; $j ++) {
						$path1 = $reasonImages[$i + $j]->guid;
						// post_title / post_excerpt
						$title1 = $reasonImages[$i + $j]->post_excerpt;
						$content1 = $reasonImages[$i + $j]->post_content;
				?>
				<div class="clearfix">
					<div class="col-md-4">
						<figure>
							<aaa href=""><img src="<?php echo $path1; ?>" alt="#"></aaa>
							<h3><aaa href=""><?php echo $title1; ?></aaa></h3>
							<p><?php echo $content1; ?></p>
						</figure>
					</div>
					<?php } ?>
				</div>
				<?php } ?>

			</div>
		</section>

		<?php
		} else if($sec == "3") {
			?>

		<!--------------------------------- Testimony Section --------------------------------->
		<section class="container-fluid team-section" data-parallax="scroll" data-image-src="<?php echo get_media_by_title(get_option('testimonial_background')) ?>">
			<div class="row content-center">
				<div class="tb-column">
					<div class="tb-cell">
						<h2>“<?php
							$custom = get_post_custom($testimony->ID);
							$image = "";
							if (has_post_thumbnail( $testimony->ID ))
								$image = wp_get_attachment_image_src( get_post_thumbnail_id( $testimony->ID ), 'single-post-thumbnail' )[0];
							echo $testimony->post_title ?>” </h2>
						<img src="<?php echo $image; ?>" alt="#" class="img-circle">
						<p>
							<span class="name"><?php echo $custom['Staff Name'][0] ?></span>
							<span class="position"><?php echo $custom['Staff Title'][0] ?></span>
						</p>
					</div>
				</div>
			</div>
		</section>

		<?php
		} else if($sec == "4") {
			?>

		<!--------------------------------- Jobs Section --------------------------------->
		<section class="container-fluid join-section">
			<div class="row content-center">
				<div class="col-md-12 clearfix">
					<h2><a name="JoinFamily">Join Our Family</a></h2>
					<p>Here are our openings. Think you fit it. Apply today</p>
					<div class="filter-by">
						<ul>
							<li class="label-title">Filter by department:</li>
							<li><a href="/career/#JoinFamily" class="<?php echo $job_type == 'Job Posting' ? "active" : "" ?>">Show all</a></li>
							<li><a href="/career/Product/#JoinFamily" class="<?php echo $job_type == 'Product' ? "active" : "" ?>">Product</a></li>
							<li><a href="/career/Marketing/#JoinFamily" class="<?php echo $job_type == 'Marketing' ? "active" : "" ?>">Marketing</a></li>
							<li><a href="/career/Content/#JoinFamily" class="<?php echo $job_type == 'Content' ? "active" : "" ?>">Content</a></li>
							<li><a href="/career/Human%20Resources/#JoinFamily" class="<?php echo $job_type == 'Human Resources' ? "active" : "" ?>">Human Resources</a></li>
						</ul>
					</div>
				</div>

				<?php
				// ----------------- Jobs Loop -----------------
				$count = count($job_list);
				for($i = 0; $i<$count; $i += 3) {

					?>
				<div class="list-job clearfix">

					<?php
					// ----------------- Triple Loop -----------------
					for($j = 0; $j < 3 && $i + $j < $count; $j ++) {
						$job1 = $job_list[$i + $j];
						$catID1 = wp_get_post_categories($job1->ID)[0];
						$job_type1 = get_post_custom($job1->ID)['Job Type'][0];

						foreach($jobCategories as $cat) {
							if($cat->cat_ID == $catID1) {
								$category = $cat;
								break;
							}
						}
						//$link = get_post_permalink($job1->ID);
						$link = "/job-posting/" . $category->slug . "/" . $job1->post_name;

						foreach($icon_list as $icon) {
							if($catID1 && $cat_icon[$catID1] === $icon->ID) {
								$catIcon1 = $icon->guid;
								break;
							}
						}
					?>

					<div class="col-md-4">
						<?php //echo $theme_base; ?><!--img/content/icon-1.jpg-->
						<a href="<?php echo $link ?>"><img src="<?php echo $catIcon1; ?>" alt=""></a>
						<h3><a href="<?php echo $link ?>"><?php echo $job1->post_title ?></a></h3>
						<span class="type-work"><?php echo $job_type1 ?></span>
					</div>

					<?php } ?>

				</div>
				<?php } ?>

				<?php //----------------- Below jobs list ----------------- ?>

				<div class="col-md-12 bottom-section">
					<div class="content-text">
						<div class="col-md-6">
							<h2>CAN’T FIND THE RIGHT JOB</h2>
							<p>If you think you’ve got something to offer but don’t see the perfect role, we’d still like to hear from you</p>
							<a href="/apply-job/" class="btn">UPLOAD RESUME</a>
						</div>
						<div class="col-md-6">
							<h2>UNIVERSITY PROGRAM</h2>
							<p>You are university students. And you love Chotot. We have something for you</p>
							<a href="#" class="btn">SEE DETAILS</a>
						</div>
					</div>
				</div>

			</div>
		</section>

		<!--------------------------------- End of Secions --------------------------------->
		<?php } } ?>

	</main>
	<footer class="container-fluid">
		<div class="row content-center">
			<div class="col-md-4 copy-right">
				Chotot © 2015 I <a href="#">PRIVACY POLICY</a>
			</div>
			<nav class="col-md-8 navbar navbar-default">
				<ul class="nav navbar-nav">
					<li><a href="#" class="active">Home</a></li>
					<li><a href="#">Careers</a></li>
					<li><a href="#">Chotot Academy</a></li>
					<li><a href="#" class="search"></a></li>
				</ul>
			</nav>
		</div>
	</footer>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script type="text/javascript" src="http://cdn.jsdelivr.net/jquery.slick/1.5.0/slick.min.js"></script>

	<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
	<script>
		(function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
			function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
			e=o.createElement(i);r=o.getElementsByTagName(i)[0];
			e.src='https://www.google-analytics.com/analytics.js';
			r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
		ga('create','UA-XXXXX-X','auto');ga('send','pageview');
	</script>



<?php get_footer(); ?>