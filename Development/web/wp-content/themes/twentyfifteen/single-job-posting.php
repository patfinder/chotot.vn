<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

require_once("common.php");

get_header(); ?>

<!--	<div id="primary" class="content-area">-->
<!--		<main id="main" class="site-main" role="main">-->

        <div id="fb-root"></div>
        <script>(function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4&appId=120360814669224";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>

		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

            $custom_fields = get_post_custom(get_the_ID());
            $job_type = $custom_fields['Job Type'][0];
            $expired_time = $custom_fields['Expired Time'][0];

            // Build Apply URL
            /*$applyUrl = home_url("apply-job");
            $applyUrl = add_query_arg("job-id", get_the_ID(), $applyUrl);*/
            $applyUrl = "/apply-job/" . get_the_ID() . "/";
        ?>

        <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <header class="container-fluid header">
            <div class="row content-center">
                <div class="col-md-3">
                    <a href="/" class="logo" style="background:url('<?php echo get_logo(); ?>')">ChoTot</a>
                </div>
                <?php chotot_menu(); ?>
            </div>

        </header>
        <main class="container-fluid sub-page">
            <div class="row content-center">
                <section class="col-md-8 main-content">
                    <div class="content">
                        <a href="/career" class="link-black"><span class="icon-back"></span>Back to job listing</a>
                        <h1><?php the_title() ?></h1>
                        <p class="position"><?php echo($job_type); ?>. Expired: <?php echo($expired_time); ?></p>
                        <?php the_content(); ?>
                    </div>
                </section>
                <aside class="col-md-4 col-right">
                    <div class="content">
                        <a href="<?php echo($applyUrl) ?>" class="btn btn-apply">APPLY NOW</a>
                        <p>Or, know someone who would be a perfect fit? Let them know!</p>
                        <!--<a href="#" class="btn share"><span class="icon-f"></span>Share via Facebook</a>-->

                        <div class="fb-share-button" data-href="<?php echo get_permalink() ?>" data-layout="button_count"></div>

                    </div>
                </aside>
                <div class="col-md-12 bottom-section">
                    <div class="content-text">
                        <div class="col-md-6">
                            <h2>CAN’T FIND THE RIGHT JOB</h2>
                            <p>If you think you’ve got something to offer but don’t see the perfect role, we’d still like to hear from you</p>
                            <a href="/apply-job/" class="btn">UPLOAD RESUME</a>
                        </div>
                        <div class="col-md-6">
                            <h2>UNIVERSITY PROGRAM</h2>
                            <p>You are university students. And you love Chotot. We have something for you</p>
                            <a href="#" class="btn">SEE DETAILS</a>
                        </div>
                    </div>
                </div>
            </div>
        </main>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://cdn.jsdelivr.net/jquery.slick/1.5.0/slick.min.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
                function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
                e=o.createElement(i);r=o.getElementsByTagName(i)[0];
                e.src='https://www.google-analytics.com/analytics.js';
                r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>



		<?php
        // End the loop.
        endwhile; ?>

		<!--</main>--><!-- .site-main -->
	<!--</div>--><!-- .content-area -->

<?php get_footer(); ?>
