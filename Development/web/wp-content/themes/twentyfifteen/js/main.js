$(document).ready( function() {
    $('.slide-life').slick({
        dots: true,
        infinite: false,
        autoplay: true,
        autoplaySpeed: 2000,
        slidesToShow: 2,
        slidesToScroll: 1,
        variableWidth:false,
        arrows: false
    });
    $('.slide-life figure').each( function(){
        var h = $(this).height();
        $(this).find('.content-hover').css({'height': h});
    });
    $('.filter-by a').on('click', function(){
        $('.filter-by a').removeClass('active');
        $(this).addClass('active');
    });
});