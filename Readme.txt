- DB file is chotot.sql
- Change DB Name in db script if needed
- The main website code is in web folder
- On Live Server Deployment, change domain name in DB:
	Table wp_options:
		siteurl -> http://chotot.vn
		home -> http://chotot.vn
	In above line, change "chotot.vn" to the actual site domain.

- Change Facebook API Key at:
	File \web\wp-content\themes\twentyfifteen\single-job-posting.php
		Search "connect.facebook.net" and replace value for "appId".

- Content pages:
	* /career/
		Main career page
	* /job-posting/product/hello-world-2
		Sample Job posting pages with details.
		From the career main page you can also scroll down to job posts at bottom to choose a job
	* /apply-job/6/
		Apply for a sample job.
